/**
 * @file
 * This is amazon_like_login.js javascript.
 * */

(function ($) {
  'use strict';
  Drupal.behaviors.amazon_like_login = {
    attach: function (context, settings) {
      $('#edit-password-fieldset-0', context).change(function () {
        $(this).closest('form').submit();
      });
      $('#edit-password-fieldset-1', context).change(function () {
        if ($('input#edit-password-fieldset-1', context).val() === '1') {
          $('input#edit-pass').attr('disabled', '');
        }
      });
      $('input#edit-pass', context).hide();
      $('input#edit-password-text', context).show();
      $('input#edit-password-text', context).focus(function () {
        $('input#edit-password-text', context).hide();
        $('input#edit-pass').show().focus();
      });
      $('input#edit-pass', context).blur(function () {
        if ($('input#edit-pass', context).val() === '') {
          $('input#edit-password-text').show();
          $('input#edit-pass', context).hide();
        }
      });
    }
  };
})(jQuery);
