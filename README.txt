AMAZON-LIKE LOGIN
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Future developments
 * Contributions are welcome!!
 * Maintainers


INTRODUCTION
------------

The Amazon-like login module converts the normal login to Amazon-like login.
It provides the option to login via registered email/username.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/neeravbm/1777308

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/1777308


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will convert the default login page to Amazon-like login.


FUTURE DEVELOPMENTS
-------------------

Features that could probably be supported but haven't been
tested/developed yet,
- Login via mobile number like amazon login.
- Provide more interactive way to update/reset user password.


CONTRIBUTIONS ARE WELCOME!!
---------------------------

Feel free to follow up in the issue queue at

https://www.drupal.org/project/issues/1777308

for any contributions, bug reports, feature requests.
Tests, feedback or comments in general are highly appreciated.


MAINTAINERS
-----------

Current maintainers:

 * Neerav Mehta (neeravbm) - https://www.drupal.org/u/neeravbm

This project has been sponsored by:
 * RED CRACKLE
   Specialized in consulting and planning of Drupal powered sites, RED CRACKLE
   offers installation, development, theming, customization, and hosting
   to get you started. Visit http://redcrackle.com for more information.
